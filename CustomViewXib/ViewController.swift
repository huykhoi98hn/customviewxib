//
//  ViewController.swift
//  CustomViewXib
//
//  Created by TTC Training on 1/15/20.
//  Copyright © 2020 TTC Training. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    
    @IBAction func btn1(_ sender: Any) {
        //TH1: truyen ca 2 closure
//        let myAlertView = MyAlertView(didYes: {
//            print("a")
//        }, didNo: {
//            print("b")
//        })
        //TH2: truyen 1 closure
        let myAlertView = MyAlertView(didYes: {
            print("a")
        })
        
        self.view.addSubview(myAlertView)
        self.view.layoutIfNeeded()
    }
    
    @IBAction func btn2(_ sender: Any) {
        
    }
}

