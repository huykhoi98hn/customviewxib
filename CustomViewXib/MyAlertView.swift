//
//  MyAlertView.swift
//  CustomViewXib
//
//  Created by TTC Training on 1/15/20.
//  Copyright © 2020 TTC Training. All rights reserved.
//

import UIKit

class MyAlertView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var askLabel: UILabel!
    @IBOutlet weak var popupView: UIView!
    var didYes: (() -> ())?
    var didNo: (() -> ())?
    @IBOutlet weak var yesBtn: UIButton!
    @IBOutlet weak var noBtn: UIButton!
    @IBOutlet weak var stackView: UIStackView!
    
    //có 2 trường hợp cần sử dụng @escaping, 1 là đa luồng, 2 là lưu trữ closure như là 1 thuộc tính, ở đây là trường hợp thứ 2, didNo không cần vì nó là kiểu Optional, không phải Function type
    init(didYes: @escaping (() -> ()), didNo: (() -> ())? = nil){
        super.init(frame: UIApplication.shared.windows.first!.frame)
        commonInit()
        initUI()
        self.didYes = didYes
        self.didNo = didNo
        //Nếu truyền 1 closure => Ẩn nút No => StackView tự động đẩy yesBtn vào giữa (UIView không tự động đẩy)
        self.noBtn.isHidden = didNo == nil
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if touches.first?.view == contentView {
            self.removeFromSuperview()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    
    func commonInit(){
        Bundle.main.loadNibNamed("MyAlertView", owner: self, options: nil)
        self.addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    func initUI(){
        popupView.layer.cornerRadius = 5
        //cho alpha < 1 có thể nhìn xuyên qua được
        self.contentView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4);
    }
    
    @IBAction func yesBtn(_ sender: Any) {
        if let didYes = didYes{
            didYes()
        }
    }
    
    @IBAction func noBtn(_ sender: Any) {
        if let didNo = didNo{
            didNo()
        }
    }
}
